#!/usr/bin/env bash

apt-mark unhold kubelet kubeadm kubectl
apt-get install -y kubelet={{ .kubeadm.version }}-00 kubeadm={{ .kubeadm.version }}-00 kubectl={{ .kubeadm.version }}-00
apt-mark hold kubelet kubeadm kubectl

# initialize k8s cluster
kubeadm init --config ./dist/deploy.sh

# Copy kubelet config file
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

# Install kube-router
kubectl apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter-all-features.yaml

# Clean up kube-proxy
kubectl -n kube-system delete ds kube-proxy
docker run --privileged -v /lib/modules:/lib/modules --net=host k8s.gcr.io/kube-proxy-amd64:v1.15.1 kube-proxy --cleanup
