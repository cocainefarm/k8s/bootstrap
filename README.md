# Setting up Kubernetes on Openstack

## Bootstrap
To bootstrap simply change the values in the cloudinit files to have your master ip and a new bootstrap token.  
Then spin up a master in openstack with the master cloudinit file and assign it the floating ip you set in the `kubeadm_custom.conf` file. Then spin up additional nodes with the node cloudinit file. They will link up automaticly.

## Kubernetes base setup
Order matters in these steps. Especially if you want to use gitlab's tiller and your own tiller.

### Persistent Volumes
We need to set up a storage class:
* `kubectl apply -f storage/default_storage_class.yaml`

### Gitlab Integration
https://gitlab.com/help/user/project/clusters/index#adding-an-existing-kubernetes-cluster

* get api url: `kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'`
* get the ca certificate: `kubectl get secret $(kubectl get secrets | grep default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode`
* apply the `gitlab_user.yaml` file to create the user for gitlab
* get it's token with: ` kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') | grep "token:" | awk '{print $2}'`

Then install the helm integration in the UI. This might take some time.
Now you may install the gitlab provided ingress etc. But because of some needed changes i recommend setup it up yourself.

### Install Helm
Now install your own helm tiller.

* apply the tiller user script `kubectl apply -f helm/tiller-rbac.yaml`
* install tiller: `helm init --service-account tiller`

### Loadbalancer
The openstack loadbalancer has servere limitations in that in can't do UDP and you would need an additional IPv4 for each service you want to expose.  
Because of this I use the `metallb` loadbalancer. [Documentation](https://metallb.universe.tf/installation/)
It's mainly:

* install metallb: `kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.7.3/manifests/metallb.yaml`
* apply metallb config

### Nginx Ingress and cert-manager
Install nginx via helm:

`helm install --name nginx-ingress stable/nginx-ingress -f nginx-ingress.yaml`

install cermanager via helm:

* install CRDs `kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.8/deploy/manifests/00-crds.yaml`
* Create namespace `kubectl create namespace cert-manager`
* disable cert validation on it `kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true`
* add jetstack repo `helm repo add jetstack https://charts.jetstack.io && helm repo update`
* install cert-manager `helm install --name cert-manager --namespace cert-manager --version v0.8.0 jetstack/cert-manager -f cert-manager.yaml`
* create cluster issuer
