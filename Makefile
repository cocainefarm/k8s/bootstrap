# -------------------------------------
#  Create a k8s cluster on openstack
# -------------------------------------

.PHONY: render install-deps install-helm install-helmfile install-gomplate files k8s gitlab helm-repo-stable helm-repo-cert-manager helm metallb ingress cert-manager storage
render:
	gomplate -c .=${PWD}/env/kubeadm.yaml -f kubeadm/deploy.sh -o dist/deploy.sh
	gomplate -c .=${PWD}/env/kubeadm.yaml -f kubeadm/config.yaml -o dist/config.yaml
	gomplate -c .=${PWD}/env/metallb.yaml -f metallb/metallb-config.yaml -o dist/metallb-config.yaml
	gomplate -c .=${PWD}/env/ingress.yaml -f ingress/nginx-ingress.yaml -o dist/nginx-ingress.yaml
	gomplate -c .=${PWD}/env/ingress.yaml -f ingress/cert-manager.yaml -o dist/cert-manager.yaml
	gomplate -c .=${PWD}/env/ingress.yaml -f ingress/cert-manager-issuer.yaml -o dist/cert-manager-issuer.yaml

install-deps: install-helm install-helmfile install-gomplate

install-helmfile:
	curl -o ${HOME}/.local/bin/helmfile https://github.com/roboll/helmfile/releases/download/v0.97.0/helmfile_linux_amd64
	chmod +x ${HOME}/.local/bin/helmfile

install-helm:
	curl -o /tmp/helm.tar.gz -sSL https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz
	tar -xf /tmp/helm.tar.gz linux-amd64/helm && mv linux-amd64/helm ${HOME}/.local/bin/helm
	rm -rf /tmp/helm.tar.gz && rmdir linux-amd64

install-gomplate:
	curl -o ${HOME}/.local/bin/gomplate -sSL https://github.com/hairyhenderson/gomplate/releases/download/v3.5.0/gomplate_linux-amd64
	chmod +x ${HOME}/.local/bin/gomplate

files:
	install ./kubeadm/kubeletargs /etc/default/kubelet

k8s: gitlab helm metallb ingress

gitlab:
	@kubectl apply -f gitlab/gitlab_user.yaml
	@echo '-------------------------------------------------\
  api url: \
  '
	@kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $$NF}'
	@echo '-------------------------------------------------\
  ca cert: \
  '
	@kubectl get secret $$(kubectl get secrets | grep default-token | awk '{print $$1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
	@echo '-------------------------------------------------\
  token: \
  '
	@kubectl -n kube-system describe secret $$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $$1}') | grep "token:" | awk '{print $$2}'

helm-repo-stable:
	helm repo add stable https://kubernetes-charts.storage.googleapis.com/

helm-repo-cert-manager:
	helm repo add jetstack https://charts.jetstack.io
	helm repo update

helm:
	helm init

metallb:
	kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.3/manifests/metallb.yaml
	kubectl apply -f dist/metallb-config.yaml

ingress: metallb cert-manager helm-repo-stable
	helm install --ingress nginx-ingress stable/nginx-ingress -f dist/nginx-ingress.yaml
	kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.8/deploy/manifests/00-crds.yaml
	kubectl create namespace cert-manager
	kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true

cert-manager: helm-repo-cert-manager
	kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml
	helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.12.0 -f dist/cert-manager.yaml
	#kubectl apply -f dist/cert-manager-issuer.yaml

storage:
	kubectl apply -f storage/default_storage_class.yaml
